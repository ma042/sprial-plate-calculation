import pandas as pd
import numpy as np
import os

#file name
file_name = "example_data_file.csv"

#file direction
file_direction = "."#use your file direction to replace the dot, such as C:/Users/data
os.chdir(file_direction)

# make the function to calculate bacteria concentration
def con_cal(df):
    """
    Calculate the bacteria concentration, reture the new dataframe. 
    Can distiguish differnet spiral plate mode. 
    Currently support E_mode and log_mode
    """
    # make the dictionary for spiral plate mode
    # E_mode 50ul
    E_mode = {"3c": 2.56,
              "3b": 6.44,
              "3a": 12.08,
              "4c": 20.36,
              "4b": 32.32,
              "4a": 50,# "*4" is for further step, the real total is 50
              "total": (50 * 4),
              "base": 50}  # the base value which is the total value

    # Log_mode 100ul
    Log_mode = {"3c": 4.32,
                "3b": 10.8,
                "3a": 21.12,
                "4c": 36.4,
                "4b": 60,
                "4a": 98.4,
                "total": (100 * 4),# "*4" is for further step, the real total is 100
                "base": 100}  # the base value which is the total value

    # make the function for one line caculation of CFU/ml
    def con_cal_row(row, Spiral_plate, Spiral_mode, Counting_region, Dilution, CFU, Plating_volume):
        """
        Calculate the concentration of one row of dataframe
        """

        # check sprial plate or not
        if row[Spiral_plate]:  # it is spiral plate
            # get spiral_mode
            if row[Spiral_mode] == "E":
                mode = E_mode
            elif row[Spiral_mode] == "Log":
                mode = Log_mode

            # calculate CFU/ml
            CFU_ml = row[CFU] * 4 / (mode[row[Counting_region]] * (
                row[Plating_volume]/mode["base"])) * (10 ** row[Dilution]) * 1000

            # if the row[CFU] = 0
            if row[CFU] == 0:
                CFU_ml = 0

        elif not row[Spiral_plate]:
            # calculate CFU/ml
            CFU_ml = row[CFU] / row[Plating_volume] * \
                (10 ** row[Dilution]) * 1000

        else:
            CFU_ml = np.nan
            print("Wrong spiral_plate boolean in")
            print(row[Spiral_plate])

        if row[CFU] == 0:
            CFU_ml = 0

        return CFU_ml

    # apply the funciton to each line
    df["CFU_ml"] = df.apply(con_cal_row, axis=1, args=(
        "Spiral_plate", "Spiral_mode", "Counting_region", "Dilution", "CFU", "Plating_volume"))

    return df

# open the data file
Data = pd.read_csv(file_name)

# calculate the CFU/ml
Data = con_cal(Data)

# save the Data
Data.to_csv(file_name, index = False)

