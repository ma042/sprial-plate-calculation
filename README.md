# Sprial plate calculation 

This project provide a script named Spiral_plate_calculating.py. You can use this script to easily calculate the bacteria concentration from the CFU counting results of spiral plates. Currently this script supports the sprial plates plated by Eddy Jet 2W spiral plater using **E-mode** or **Log-mode**.

The raw data should be stored in a csv file with the following columns. 
|Spiral_plate|Spiral_mode|Counting_region|Dilution|CFU|Plating_volume|
|------------|-----------|---------------|--------|---|--------------|


* Spiral_plate: using spiral plate or not. (TRUE/FALSE)
* Spiral_mode: which mode is used for spiral plate. (Log/E)
* Counting_region: If using spiral plate, the counting region up to which region. (3a/3b/3c/4a/4b/4c/total)
* Dilution: the 10-fold dilution times of the sample before it being plated on the plate.
* CFU: the count of the colonies in the counting region.
* Plating_Volume: the volume (μl) of the diluted sample plated on the plate. (e.g. for E-mode50μl is 50)

In the script, you need to change the data file name and direction. Then, by running the whole script, a new column *CFU_ml* will be saved to your data file which is the concentration. 

You can also find a example data file in this project.
